package org.apache.spark.examples.tpch

import org.apache.spark.sql.functions.first
import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.functions.udf

/**
 * TPC-H Query 19
 * Savvas Savvides <ssavvides@us.ibm.com>
 *
 */
class Q63 extends TpchQuery {

  import sqlContext.implicits._

  override def execute(): Unit = {

    val sm = udf { (x: String) => x.matches("SM CASE|SM BOX|SM PACK|SM PKG") }
    val md = udf { (x: String) => x.matches("MED BAG|MED BOX|MED PKG|MED PACK") }
    val lg = udf { (x: String) => x.matches("LG CASE|LG BOX|LG PACK|LG PKG") }

    val decrease = udf { (x: Double, y: Double) => x * (1 - y) }

    // project part and lineitem first?
    val res = part.join(lineitem, $"l_partkey" === $"p_partkey")
      .filter(($"l_shipmode" === "AIR" || $"l_shipmode" === "AIR REG") &&
        $"l_shipinstruct" === "DELIVER IN PERSON")
      .filter(
        (($"p_brand" === "Brand#31") &&
          sm($"p_container") &&
          $"l_quantity" >= 7 && $"l_quantity" <= 17 &&
          $"p_size" >= 1 && $"p_size" <= 5) ||
          (($"p_brand" === "Brand#15") &&
            md($"p_container") &&
            $"l_quantity" >= 13 && $"l_quantity" <= 23 &&
            $"p_size" >= 1 && $"p_size" <= 10) ||
            (($"p_brand" === "Brand#11") &&
              lg($"p_container") &&
              $"l_quantity" >= 24 && $"l_quantity" <= 34 &&
              $"p_size" >= 1 && $"p_size" <= 15))
      .select(decrease($"l_extendedprice", $"l_discount").as("volume"))
      .agg(sum("volume"))
    res.collect()
    outputDF(res)

  }

}

