package org.apache.spark.examples.tpch

import org.apache.spark.sql.functions.count
import org.apache.spark.sql.functions.udf

/**
 * TPC-H Query 13
 * Savvas Savvides <ssavvides@us.ibm.com>
 *
 */
class Q145 extends TpchQuery {

  import sqlContext.implicits._

  override def execute(): Unit = {

    val special = udf { (x: String) => x.matches(".*express.*accounts.*") }

    val res = customer.join(order, $"c_custkey" === order("o_custkey")
      && !special(order("o_comment")), "left_outer")
      .groupBy($"o_custkey")
      .agg(count($"o_orderkey").as("c_count"))
      .groupBy($"c_count")
      .agg(count($"o_custkey").as("custdist"))
      .sort($"custdist".desc, $"c_count".desc)
    res.collect()
    outputDF(res)

  }

}

