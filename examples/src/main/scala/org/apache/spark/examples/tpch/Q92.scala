package org.apache.spark.examples.tpch

import org.apache.spark.sql.functions.count

/**
 * TPC-H Query 4
 * Savvas Savvides <ssavvides@us.ibm.com>
 *
 */
class Q92 extends TpchQuery {

  import sqlContext.implicits._

  override def execute(): Unit = {

    val forders = order.filter($"o_orderdate" >= "1994-05-01" && $"o_orderdate" < "1994-08-01")
    val flineitems = lineitem.filter($"l_commitdate" < $"l_receiptdate")
      .select($"l_orderkey")
      .distinct

    val res = flineitems.join(forders, $"l_orderkey" === forders("o_orderkey"))
      .groupBy($"o_orderpriority")
      .agg(count($"o_orderpriority"))
      .sort($"o_orderpriority")

   // println(res.queryExecution.sparkPlan.toString)
    res.collect()

    outputDF(res)

  }

}

