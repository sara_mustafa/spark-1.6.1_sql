package org.apache.spark.examples.tpch

import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.functions.udf

/**
 * TPC-H Query 14
 * Savvas Savvides <ssavvides@us.ibm.com>
 *
 */
class Q58 extends TpchQuery {

  import sqlContext.implicits._

  override def execute(): Unit = {

    val reduce = udf { (x: Double, y: Double) => x * (1 - y) }
    val promo = udf { (x: String, y: Double) => if (x.startsWith("PROMO")) y else 0 }

    val res = part.join(lineitem, $"l_partkey" === $"p_partkey" &&
      $"l_shipdate" >= "1997-06-01" && $"l_shipdate" < "1997-07-01")
      .select($"p_type", reduce($"l_extendedprice", $"l_discount").as("value"))
      .agg(sum(promo($"p_type", $"value")) * 100 / sum($"value"))
    res.collect()
    outputDF(res)

  }

}

