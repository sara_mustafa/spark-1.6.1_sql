package org.apache.spark.examples.tpch

import java.text.{ParseException, SimpleDateFormat}
import java.util.Date

import org.apache.commons.lang3.StringUtils
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.scheduler._
import org.apache.spark.sql.functions.min
import scala.collection.mutable
import scala.io.Source
import scala.reflect.runtime.universe
import org.apache.spark.sql.{SQLContext, DataFrame}
import java.io.{FileWriter, File}
import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.WildcardFileFilter

case class Customer(
  c_custkey: Int,
  c_name: String,
  c_address: String,
  c_nationkey: Int,
  c_phone: String,
  c_acctbal: Double,
  c_mktsegment: String,
  c_comment: String)

case class Lineitem(
  l_orderkey: Int,
  l_partkey: Int,
  l_suppkey: Int,
  l_linenumber: Int,
  l_quantity: Double,
  l_extendedprice: Double,
  l_discount: Double,
  l_tax: Double,
  l_returnflag: String,
  l_linestatus: String,
  l_shipdate: String,
  l_commitdate: String,
  l_receiptdate: String,
  l_shipinstruct: String,
  l_shipmode: String,
  l_comment: String)

case class Nation(
  n_nationkey: Int,
  n_name: String,
  n_regionkey: Int,
  n_comment: String)

case class Order(
  o_orderkey: Int,
  o_custkey: Int,
  o_orderstatus: String,
  o_totalprice: Double,
  o_orderdate: String,
  o_orderpriority: String,
  o_clerk: String,
  o_shippriority: Int,
  o_comment: String)

case class Part(
  p_partkey: Int,
  p_name: String,
  p_mfgr: String,
  p_brand: String,
  p_type: String,
  p_size: Int,
  p_container: String,
  p_retailprice: Double,
  p_comment: String)

case class Partsupp(
  ps_partkey: Int,
  ps_suppkey: Int,
  ps_availqty: Int,
  ps_supplycost: Double,
  ps_comment: String)

case class Region(
  r_regionkey: Int,
  r_name: String,
  r_comment: String)

case class Supplier(
  s_suppkey: Int,
  s_name: String,
  s_address: String,
  s_nationkey: Int,
  s_phone: String,
  s_acctbal: Double,
  s_comment: String)

/**
 * Parent class for TPC-H queries.
 *
 * Defines schemas for tables and reads pipe ("|") separated text files into these tables.
 *
 * Savvas Savvides <ssavvides@us.ibm.com>
 *
 */
object Conf{
  val conf = new SparkConf().setAppName("Mysql test").setMaster("local")
 //  val conf = new SparkConf().setAppName("Mysql test")
    //.set("spark.executor.memory","1g").set("spark.executor.instances","2").set("spark.executor.cores","1")
  val sc = new SparkContext(conf)
  val sqlContext = new org.apache.spark.sql.SQLContext(sc)
}

abstract class TpchQuery {

  // read files from local FS
  // val INPUT_DIR = "file://" + new File(".").getAbsolutePath() + "/dbgen"

  // read from hdfs
//  val INPUT_DIR: String = "hdfs://localhost:54310/user/hduser/smallset"
  val INPUT_DIR: String = "hdfs://node1:54310/user/tpch"
  // if set write results to hdfs, if null write to stdout
  val OUTPUT_DIR: String = ""

  // get the name of the class excluding dollar signs and package
 // val className = this.getClass.getName.split("\\.").last.replaceAll("\\$", "")

  // create spark context and set class name as the app name
//  val sc = new SparkContext(new SparkConf().setAppName("TPC-H " + className))

  // convert an RDDs to a DataFrames
 // val sqlContext = new org.apache.spark.sql.SQLContext(sc)



  import org.apache.spark.sql.functions._
  val sqlContext = Conf.sqlContext
  import sqlContext.implicits._

//  val customer = Conf.sc.textFile(INPUT_DIR + "/customer.tbl.1").map(_.split('|')).map(p => Customer(p(0).trim.toInt, p(1).trim, p(2).trim, p(3).trim.toInt, p(4).trim, p(5).trim.toDouble, p(6).trim, p(7).trim)).toDF()
//  val lineitem = Conf.sc.textFile(INPUT_DIR + "/lineitem.tbl.1").map(_.split('|')).map(p => Lineitem(p(0).trim.toInt, p(1).trim.toInt, p(2).trim.toInt, p(3).trim.toInt, p(4).trim.toDouble, p(5).trim.toDouble, p(6).trim.toDouble, p(7).trim.toDouble, p(8).trim, p(9).trim, p(10).trim, p(11).trim, p(12).trim, p(13).trim, p(14).trim, p(15).trim)).toDF()
//  val nation = Conf.sc.textFile(INPUT_DIR + "/nation.tbl").map(_.split('|')).map(p => Nation(p(0).trim.toInt, p(1).trim, p(2).trim.toInt, p(3).trim)).toDF()
//  val region = Conf.sc.textFile(INPUT_DIR + "/region.tbl").map(_.split('|')).map(p => Region(p(0).trim.toInt, p(1).trim, p(1).trim)).toDF()
//  val order = Conf.sc.textFile(INPUT_DIR + "/orders.tbl.1").map(_.split('|')).map(p => Order(p(0).trim.toInt, p(1).trim.toInt, p(2).trim, p(3).trim.toDouble, p(4).trim, p(5).trim, p(6).trim, p(7).trim.toInt, p(8).trim)).toDF()
//  val part = Conf.sc.textFile(INPUT_DIR + "/part.tbl.1").map(_.split('|')).map(p => Part(p(0).trim.toInt, p(1).trim, p(2).trim, p(3).trim, p(4).trim, p(5).trim.toInt, p(6).trim, p(7).trim.toDouble, p(8).trim)).toDF()
//  val partsupp = Conf.sc.textFile(INPUT_DIR + "/partsupp.tbl.1").map(_.split('|')).map(p => Partsupp(p(0).trim.toInt, p(1).trim.toInt, p(2).trim.toInt, p(3).trim.toDouble, p(4).trim)).toDF()
//  val supplier = Conf.sc.textFile(INPUT_DIR + "/supplier.tbl.1").map(_.split('|')).map(p => Supplier(p(0).trim.toInt, p(1).trim, p(2).trim, p(3).trim.toInt, p(4).trim, p(5).trim.toDouble, p(6).trim)).toDF()

  
  val customer = sc.textFile(INPUT_DIR + "/customer/").map(_.split('|')).map(p => Customer(p(0).trim.toInt, p(1).trim, p(2).trim, p(3).trim.toInt, p(4).trim, p(5).trim.toDouble, p(6).trim, p(7).trim)).toDF()
  val lineitem = sc.textFile(INPUT_DIR + "/lineitem/").map(_.split('|')).map(p => Lineitem(p(0).trim.toInt, p(1).trim.toInt, p(2).trim.toInt, p(3).trim.toInt, p(4).trim.toDouble, p(5).trim.toDouble, p(6).trim.toDouble, p(7).trim.toDouble, p(8).trim, p(9).trim, p(10).trim, p(11).trim, p(12).trim, p(13).trim, p(14).trim, p(15).trim)).toDF()
  val nation = sc.textFile(INPUT_DIR + "/nation.tbl").map(_.split('|')).map(p => Nation(p(0).trim.toInt, p(1).trim, p(2).trim.toInt, p(3).trim)).toDF()
  val region = sc.textFile(INPUT_DIR + "/region.tbl").map(_.split('|')).map(p => Region(p(0).trim.toInt, p(1).trim, p(1).trim)).toDF()
  val order = sc.textFile(INPUT_DIR + "/orders/").map(_.split('|')).map(p => Order(p(0).trim.toInt, p(1).trim.toInt, p(2).trim, p(3).trim.toDouble, p(4).trim, p(5).trim, p(6).trim, p(7).trim.toInt, p(8).trim)).toDF()
  val part = sc.textFile(INPUT_DIR + "/part/").map(_.split('|')).map(p => Part(p(0).trim.toInt, p(1).trim, p(2).trim, p(3).trim, p(4).trim, p(5).trim.toInt, p(6).trim, p(7).trim.toDouble, p(8).trim)).toDF()
  val partsupp = sc.textFile(INPUT_DIR + "/partsupp/").map(_.split('|')).map(p => Partsupp(p(0).trim.toInt, p(1).trim.toInt, p(2).trim.toInt, p(3).trim.toDouble, p(4).trim)).toDF()
  val supplier = sc.textFile(INPUT_DIR + "/supplier/").map(_.split('|')).map(p => Supplier(p(0).trim.toInt, p(1).trim, p(2).trim, p(3).trim.toInt, p(4).trim, p(5).trim.toDouble, p(6).trim)).toDF()

  customer.registerTempTable("customer")
  lineitem.registerTempTable("lineitem")
  nation.registerTempTable("nation")
  region.registerTempTable("region")
  order.registerTempTable("order")
  part.registerTempTable("part")
  partsupp.registerTempTable("partsupp")
  supplier.registerTempTable("supplier")


  /**
   *  implemented in children classes and hold the actual query
   */
  def execute(): Unit

  def printToFile(f: java.io.FileWriter)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
  }
  def outputDF(res: DataFrame): Unit = {
    var p = res.queryExecution.sparkPlan
    p.buildStats(p)

    var c = Conf.sc.dagScheduler.histograms
    c.foreach {keyVal =>
      printToFile(new FileWriter("hist-"+keyVal._1+".txt")) {
       p=>  p.write(keyVal._2.toString+"\n")
      }
    }
  }

}

object TpchQuery {
//  Conf.sc.addSparkListener(new StatsReportListener() {
//    override def onJobStart(jobStart: SparkListenerJobStart): Unit = {
//
//      println("------ Job start " +jobStart.jobId)
//        println("job start time "+jobStart.time)
//        println("job properties "+ jobStart.properties)
//            println("stages nums "+jobStart.stageInfos.length)
//            for (i <- 0 to jobStart.stageInfos.length-1){
//              println("*******************************Stage*****************************")
//
//              val stage = jobStart.stageInfos(i)
//              println("stage id "+stage.stageId)
//            //  println("stage completion time "+stage.completionTime)
//              println("stages num "+  stage.stageId +  "   tasks num "+stage.numTasks)
//              stage.rddInfos.foreach(r => println(r.toString+"\n-----------------"))
//              println("*******************************stage End*****************************")
//
//        }
////      println("------ Job start ======================")
//     Conf.sc.cancelJob(jobStart.jobId)
////     println("tasks num "+stage.numTasks)
////      stage.rddInfos.foreach(r => println(r.toString))
//    }
//    override def onStageCompleted(stageCompleted: SparkListenerStageCompleted): Unit = {
////   println("------ Stage Completed "+ stageCompleted.toString)
////
////      stageCompleted.stageInfo.rddInfos.foreach(r => println(r.toString))
//    }
////    override  def onStageSubmitted(stageSubmitted: SparkListenerStageSubmitted): Unit = {
////      println("stage submitted "+ stageSubmitted.stageInfo.stageId)
////    }
//  }

//  )
  /**
   * Execute query reflectively
   */
  def printToFile(f: java.io.FileWriter)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
  }
  def executeQuery(queryNo: Int): Unit = {

//    var x=0
//    for (j <- 1 to 22){
//      println("----------- Execute Query "+(j+x))
//      loadHistogram()
//      Class.forName(f"org.apache.spark.examples.tpch.Q${j+x}%02d").newInstance.asInstanceOf[{ def execute }].execute
//    }
//
//      x+=22
//    for (i <- 2 to 6){
//      for (j <- 1 to 22){
//        println("----------- Execute Query "+(j+x))
//        loadHistogram()
//        Class.forName(f"org.apache.spark.examples.tpch.Q${j+x}").newInstance.asInstanceOf[{ def execute }].execute
//      }
//      x+=22
//    }
    loadHistogram()
    Class.forName(f"org.apache.spark.examples.tpch.Q${queryNo}%02d").newInstance.asInstanceOf[{ def execute }].execute
 
  }
  def getListOfFiles(dir: File): List[File] = {
    dir.listFiles.filter(_.isFile).toList.filter { file =>
      file.getName.startsWith("hist-")
    }
  }
  val df = new SimpleDateFormat("yyyy-MM-dd")

  def validDate(date: String) = try {
    df.setLenient(false)
    df.parse(date)
    true
  } catch {
    case e: ParseException => false
  }
  def loadHistogram() ={
    val  dir:File = new File("/home/sara/Sources/spark-1.6.1")
  //  val  dir:File = new File("/home/ubuntu/spark-1.6.1")
    val files = dir.listFiles.filter(_.isFile).toList.filter { file =>
      file.getName.startsWith("hist-")
    }

    for (f <- files){
      var typeKnown = false
      val colName = f.getName.substring(f.getName.indexOf('-')+1,f.getName.indexOf(".txt"))

      for (line <- Source.fromFile(f).getLines()) {
        if (line.length > 0 ){
          val splits = line.split(":")
          if (!typeKnown){
            if(splits.length ==2){
              var hist = new DistinctHistogram(0)
              hist.buckets.put(splits(0),splits(1).toInt)
              Conf.sc.dagScheduler.histograms.put(colName, hist)
              typeKnown = true
            } else {
              if (validDate(splits(0))){
                var hist = new DateHistogram(0, new Date(), new Date())
                hist.buckets.append(new DateBucket(df.parse(splits(0)),df.parse(splits(1)),splits(2).toDouble))
                Conf.sc.dagScheduler.histograms.put(colName, hist)
                typeKnown = true
              } else {
                var hist = new RangeHistogram(0,0,0)
                hist.buckets.append(new Bucket(splits(0).toInt, splits(1).toInt, splits(2).toDouble))
                Conf.sc.dagScheduler.histograms.put(colName, hist)
                typeKnown = true
              }
            }
          } else {
            var hist:Histogram = Conf.sc.dagScheduler.histograms.get(colName).get
            if (hist.isInstanceOf[DistinctHistogram]){
              hist.asInstanceOf[DistinctHistogram].addBucket(splits(0),splits(1).toInt)
            } else if (hist.isInstanceOf[DateHistogram]){
              hist.asInstanceOf[DateHistogram].buckets.append(new DateBucket(df.parse(splits(0)),df.parse(splits(1)),splits(2).toDouble))
            } else if (hist.isInstanceOf[RangeHistogram])
              hist.asInstanceOf[RangeHistogram].buckets.append(new Bucket(splits(0).toInt, splits(1).toInt, splits(2).toDouble))
          }
        }
      }

    }

  }

  def main(args: Array[String]): Unit = {
    executeQuery(6)
  }
}
