package org.apache.spark.examples.tpch

import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.functions.udf

/**
 * TPC-H Query 6
 * Savvas Savvides <ssavvides@us.ibm.com>
 *
 */
class Q116 extends TpchQuery {

  import sqlContext.implicits._

  override def execute(): Unit = {

    val res = lineitem.filter($"l_shipdate" >= "1996-01-01" && $"l_shipdate" < "1997-01-01" && $"l_discount" >= 0.03 && $"l_discount" <= 0.05 && $"l_quantity" < 25)
      .agg(sum($"l_extendedprice" * $"l_discount"))
    res.collect()
    outputDF(res)

  }

}

