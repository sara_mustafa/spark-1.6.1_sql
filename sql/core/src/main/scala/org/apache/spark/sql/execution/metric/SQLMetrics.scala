/*
* Licensed to the Apache Software Foundation (ASF) under one or more
* contributor license agreements.  See the NOTICE file distributed with
* this work for additional information regarding copyright ownership.
* The ASF licenses this file to You under the Apache License, Version 2.0
* (the "License"); you may not use this file except in compliance with
* the License.  You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package org.apache.spark.sql.execution.metric

import java.text.SimpleDateFormat
import java.util.Date

import org.apache.spark.scheduler.Histogram
import org.apache.spark.util.Utils
import org.apache.spark.{GrowableAccumulableParam, Accumulable, AccumulableParam, SparkContext}

import scala.collection.mutable
import org.apache.spark.serializer.JavaSerializer
import scala.collection.mutable.{ HashMap => MutableHashMap }
import org.apache.spark.{AccumulableParam, SparkConf}

/**
 * Create a layer for specialized metric. We cannot add `@specialized` to
 * `Accumulable/AccumulableParam` because it will break Java source compatibility.
 *
 * An implementation of SQLMetric should override `+=` and `add` to avoid boxing.
 */
private[sql] abstract class SQLMetric[R <: SQLMetricValue[T], T](name: String, val param: SQLMetricParam[R, T])
  extends Accumulable[R, T](param.zero, param, Some(name), true) {

  def reset(): Unit = {
    this.value = param.zero
  }
}

/**
 * Create a layer for specialized metric. We cannot add `@specialized` to
 * `Accumulable/AccumulableParam` because it will break Java source compatibility.
 */
private[sql] trait SQLMetricParam[R <: SQLMetricValue[T], T] extends AccumulableParam[R, T] {

  /**
   * A function that defines how we aggregate the final accumulator results among all tasks,
   * and represent it in string for a SQL physical operator.
   */
  val stringValue: Seq[T] => String

  def zero: R
}


/**
 * Create a layer for specialized metric. We cannot add `@specialized` to
 * `Accumulable/AccumulableParam` because it will break Java source compatibility.
 */
private[sql] trait SQLMetricValue[T] extends Serializable {

  def value: T

  override def toString: String = {
    if (value.isInstanceOf[mutable.HashMap[String,Long]] || value.isInstanceOf[mutable.Map[String,Long]]){
      return value.asInstanceOf[mutable.HashMap[String,Long]].head.toString()
    }
    else
      value.toString
  }
}

/**
 * A wrapper of Long to avoid boxing and unboxing when using Accumulator
 */
private[sql] class LongSQLMetricValue(private var _value : Long) extends SQLMetricValue[Long] {

  def add(incr: Long): LongSQLMetricValue = {
    _value += incr
    this
  }

  // Although there is a boxing here, it's fine because it's only called in SQLListener
  override def value: Long = _value

  def set(v: Long): LongSQLMetricValue = {
    _value = v
    this
  }
}

/**
 * created by sara
 * @param _value
 */
private[sql] class MaxDateSQLMetricValue(private var _value :Date) extends SQLMetricValue[Date] {

  //val df = new SimpleDateFormat("yyyy-MM-dd")

  def add(incr: Date): MaxDateSQLMetricValue = {
    if (incr.after(_value))
      _value = incr
    this
  }
  // Although there is a boxing here, it's fine because it's only called in SQLListener
  override def value: Date = _value

  def set(v: Date): MaxDateSQLMetricValue = {
    _value = v
    this
  }
}

/**
 * created by sara
 * @param _value
 */
private[sql] class MinDateSQLMetricValue(private var _value :Date) extends SQLMetricValue[Date] {

  val df = new SimpleDateFormat("yyyy-MM-dd")
  def add(incr: Date): MinDateSQLMetricValue = {
    if (incr.before(_value))
      _value = incr
    this
  }
  // Although there is a boxing here, it's fine because it's only called in SQLListener
  override def value: Date = _value

  def set(v: Date): MinDateSQLMetricValue = {
    _value = v
    this
  }
}

/**
 * created by sara
 * @param _value
 */
private[sql] class MaxSQLMetricValue(private var _value :Long) extends SQLMetricValue[Long] {

  def add(incr: Long): MaxSQLMetricValue = {
    if (incr > _value)
      _value = incr
    this
  }
  // Although there is a boxing here, it's fine because it's only called in SQLListener
  override def value: Long = _value

  def set(v: Long): MaxSQLMetricValue = {
    _value = v
    this
  }
}

/**
 * created by sara
 * @param _value
 */
private[sql] class MinSQLMetricValue(private var _value :Long) extends SQLMetricValue[Long] {

  def add(incr: Long): MinSQLMetricValue = {
    if (incr < _value)
      _value = incr
    this
  }
  // Although there is a boxing here, it's fine because it's only called in SQLListener
  override def value: Long = _value

  def set(v: Long): MinSQLMetricValue = {
    _value = v
    this
  }
}

/**
 * created by sara
 * @param _value
 */

private[sql] class HashMapSQLMetricValue(var _value : mutable.HashMap[String,Long]) extends SQLMetricValue[mutable.HashMap[String,Long]] {

  def add(incr :mutable.HashMap[String,Long]): HashMapSQLMetricValue = {
    incr.foreach(keyVal => {
      if (_value.contains(keyVal._1))
        _value.put(keyVal._1, _value.get(keyVal._1).get+keyVal._2)
      else
        _value.put(keyVal._1,keyVal._2)
    })
    this

  }

  // Although there is a boxing here, it's fine because it's only called in SQLListener
  override def value: mutable.HashMap[String,Long] = _value

  def set(v: mutable.HashMap[String,Long]): HashMapSQLMetricValue= {
    _value = v
    this
  }
}

/**
 * A specialized long Accumulable to avoid boxing and unboxing when using Accumulator's
 * `+=` and `add`.
 */
private[sql] class LongSQLMetric private[metric](name: String, param: LongSQLMetricParam)
  extends SQLMetric[LongSQLMetricValue, Long](name, param) {

  override def +=(term: Long): Unit = {
    localValue.add(term)
  }

  override def add(term: Long): Unit = {
    localValue.add(term)
  }

  def set(term: Long): Unit = {
    localValue.set(term)
  }
}

/**
 * created by sara
 * @param name
 * @param param
 */
private[sql] class MaxDateSQLMetric private[metric](name: String, param:MaxDateSQLMetricParam)
  extends SQLMetric[MaxDateSQLMetricValue, Date](name, param) {

  override def +=(term: Date): Unit = {
    if (term.after(localValue.value))
      localValue.set(term)
  }

  override def add(term: Date): Unit = {
    localValue.add(term)
  }

  def set(term: Date): Unit = {
    localValue.set(term)
  }
}

/**
 * created by sara
 * @param name
 * @param param
 */
private[sql] class MinDateSQLMetric private[metric](name: String, param: MinDateSQLMetricParam)
  extends SQLMetric[MinDateSQLMetricValue, Date](name, param) {

  override def +=(term: Date): Unit = {
    if (term.before(localValue.value))
      localValue.set(term)
  }

  override def add(term: Date): Unit = {
    localValue.add(term)
  }

  def set(term: Date): Unit = {
    localValue.set(term)
  }
}

/**
 * created by sara
 * @param name
 * @param param
 */
private[sql] class MaxSQLMetric private[metric](name: String, param: MaxSQLMetricParam)
  extends SQLMetric[MaxSQLMetricValue, Long](name, param) {

  override def +=(term: Long): Unit = {
    localValue.add(term)
  }

  override def add(term: Long): Unit = {
    localValue.add(term)
  }

  def set(term: Long): Unit = {
    localValue.set(term)
  }
}

/**
 * created by sara
 * @param name
 * @param param
 */
private[sql] class MinSQLMetric private[metric](name: String, param: MinSQLMetricParam)
  extends SQLMetric[MinSQLMetricValue, Long](name, param) {

  override def +=(term: Long): Unit = {
    localValue.add(term)
  }

  override def add(term: Long): Unit = {
    localValue.add(term)
  }

  def set(term: Long): Unit = {
    localValue.set(term)
  }
}

/**
 * created by sara
 * @param name
 * @param param
 */
private[sql] class HashMapSQLMetric private[metric](name: String, param: HashMapSQLMetricParam)
  extends SQLMetric[HashMapSQLMetricValue,mutable.HashMap[String,Long]](name, param) {

  override def +=(term: mutable.HashMap[String,Long]): Unit = {
    localValue.add(term)
  }

  override def add(term: mutable.HashMap[String,Long]): Unit = {
    localValue.add(term)
  }

  def set(term: mutable.HashMap[String,Long]): Unit = {
    localValue.set(term)
  }
}


private class LongSQLMetricParam(val stringValue: Seq[Long] => String, initialValue: Long)
  extends SQLMetricParam[LongSQLMetricValue, Long] {

  override def addAccumulator(r: LongSQLMetricValue, t: Long): LongSQLMetricValue = r.add(t)

  override def addInPlace(r1: LongSQLMetricValue, r2: LongSQLMetricValue): LongSQLMetricValue =
    r1.add(r2.value)

  override def zero(initialValue: LongSQLMetricValue): LongSQLMetricValue = zero

  override def zero: LongSQLMetricValue = new LongSQLMetricValue(initialValue)
}

/**
 * created by sara
 * @param stringValue
 * @param initialValue
 */
private class MaxSQLMetricParam(val stringValue: Seq[Long] => String, initialValue: Long)
  extends SQLMetricParam[MaxSQLMetricValue, Long] {

  override def addAccumulator(r: MaxSQLMetricValue, t: Long): MaxSQLMetricValue = r.add(t)

  override def addInPlace(r1: MaxSQLMetricValue, r2: MaxSQLMetricValue): MaxSQLMetricValue =
    r1.add(r2.value)

  override def zero(initialValue: MaxSQLMetricValue): MaxSQLMetricValue = zero

  override def zero: MaxSQLMetricValue = new MaxSQLMetricValue(initialValue)
}

/**
 * created by sara
 * @param stringValue
 * @param initialValue
 */
private class MinSQLMetricParam(val stringValue: Seq[Long] => String, initialValue: Long)
  extends SQLMetricParam[MinSQLMetricValue, Long] {

  override def addAccumulator(r: MinSQLMetricValue, t: Long): MinSQLMetricValue = r.add(t)

  override def addInPlace(r1: MinSQLMetricValue, r2: MinSQLMetricValue): MinSQLMetricValue =
    r1.add(r2.value)

  override def zero(initialValue: MinSQLMetricValue): MinSQLMetricValue = zero

  override def zero: MinSQLMetricValue = new MinSQLMetricValue(initialValue)
}

/**
 * created by sara
 * @param stringValue
 * @param initialValue
 */
private class MaxDateSQLMetricParam(val stringValue: Seq[Date] => String, initialValue: Date)
  extends SQLMetricParam[MaxDateSQLMetricValue, Date] {
  val format = new java.text.SimpleDateFormat("yyyy-MM-dd")
  var initial=""

  override def addAccumulator(r: MaxDateSQLMetricValue, t: Date): MaxDateSQLMetricValue = r.add(t)

  override def addInPlace(r1: MaxDateSQLMetricValue, r2: MaxDateSQLMetricValue): MaxDateSQLMetricValue ={
    if(r1.toString == initial && r2.toString != initial)
      r1.set(r2.value)
    else {
      if (r2.toString != initial)
        r1.add(r2.value)
      else
        r1
    }
  }
  override def zero(initialValue: MaxDateSQLMetricValue): MaxDateSQLMetricValue = zero

  override def zero: MaxDateSQLMetricValue = {
    //var d = new  Date()
    format.setLenient(false)
    var d =  format.parse("1440-01-01")
    initial = d.toString
    new MaxDateSQLMetricValue(d)
  }
}


/**
 * created by sara
 * @param stringValue
 * @param initialValue
 */
private class MinDateSQLMetricParam(val stringValue: Seq[Date] => String, initialValue: Date)
  extends SQLMetricParam[MinDateSQLMetricValue, Date] {
  val format = new java.text.SimpleDateFormat("yyyy-MM-dd")
  var initial=""

  override def addAccumulator(r: MinDateSQLMetricValue, t: Date): MinDateSQLMetricValue = r.add(t)

  override def addInPlace(r1: MinDateSQLMetricValue, r2: MinDateSQLMetricValue): MinDateSQLMetricValue ={
    if(r1.toString == initial && r2.toString != initial)
      r1.set(r2.value)
    else {
      if ( r2.toString != initial)
        r1.add(r2.value)
      else
        r1
    }
  }
  override def zero(initialValue: MinDateSQLMetricValue): MinDateSQLMetricValue = zero

  override def zero: MinDateSQLMetricValue = {
    var d = new  Date()
    initial = d.toString
    new MinDateSQLMetricValue(d)
  }
}

/**
 * created by sara

 */
//private class StatisticsSQLMetricParam(val stringValue: Seq[mutable.HashMap[String,Long]] => String, initialValue: mutable.HashMap[String,Long])
//  extends SQLMetricParam[StatisticsSQLMetricValue, mutable.HashMap[String,Long]] {
//
//
//  override def addAccumulator(r: StatisticsSQLMetricValue, t: mutable.HashMap[String,Long]): StatisticsSQLMetricValue = r.add(t)
//
//  override def addInPlace(r1: StatisticsSQLMetricValue, r2: StatisticsSQLMetricValue): StatisticsSQLMetricValue ={
//    r1.add(r2.value)
//  }
//  override def zero(initialValue: StatisticsSQLMetricValue): StatisticsSQLMetricValue = zero
//
//  override def zero: StatisticsSQLMetricValue = {
//    new StatisticsSQLMetricValue(initialValue)
//  }
//}

private class HashMapSQLMetricParam(val stringValue: Seq[mutable.HashMap[String,Long]] => String, initialValue: mutable.HashMap[String,Long] with
  mutable.SynchronizedMap[String, Long] with Serializable with scala.collection.generic.Clearable) extends SQLMetricParam[HashMapSQLMetricValue,mutable.HashMap[String,Long]] {

  def addAccumulator(acc: HashMapSQLMetricValue, elem: mutable.HashMap[String, Long]): HashMapSQLMetricValue = {
    synchronized{
      acc.add(elem)
    }

  }

  /*
   * This method is allowed to modify and return the first value for efficiency.
   *
   * @see org.apache.spark.GrowableAccumulableParam.addInPlace(r1: R, r2: R): R
   */
  def addInPlace(acc1: HashMapSQLMetricValue, acc2: HashMapSQLMetricValue): HashMapSQLMetricValue= {
    //acc2._value.foreach(elem => addAccumulator(acc1, elem))
    acc1.add(acc2.value)
    acc1
  }

  /*
   * @see org.apache.spark.GrowableAccumulableParam.zero(initialValue: R): R
   */
  def zero: HashMapSQLMetricValue = {
//    val ser = new JavaSerializer(new SparkConf(false)).newInstance()
//    val copy = ser.deserialize[mutable.HashMap[String,Long] with
//      mutable.SynchronizedMap[String, Long]](ser.serialize(initialValue))
//    copy.clear()
//    new HashMapSQLMetricValue(copy)
    println("--------------- SQL metric create hashmap")
    val metric = new HashMapSQLMetricValue(initialValue)
    val ser = new JavaSerializer(new SparkConf(false)).newInstance()
    val copy = ser.deserialize[HashMapSQLMetricValue](ser.serialize(metric))
    copy._value.clear()
    copy
  }

  override def zero(initialValue: HashMapSQLMetricValue): HashMapSQLMetricValue = zero

}
private[sql] object SQLMetrics {

  private def createLongMetric(
                                sc: SparkContext,
                                name: String,
                                stringValue: Seq[Long] => String,
                                initialValue: Long): LongSQLMetric = {
    val param = new LongSQLMetricParam(stringValue, initialValue)
    val acc = new LongSQLMetric(name, param)
    sc.cleaner.foreach(_.registerAccumulatorForCleanup(acc))
    acc
  }

  def createLongMetric(sc: SparkContext, name: String): LongSQLMetric = {
    createLongMetric(sc, name, _.sum.toString, 0L)
  }

  /**
   * created by sara
   * @param sc
   * @param name
   * @param stringValue
   * @param initialValue
   * @return
   */
  def createMaxDateMetric(sc: SparkContext, name: String,stringValue: Seq[Date] => String,
                          initialValue: Date): MaxDateSQLMetric = {
    val param = new MaxDateSQLMetricParam(stringValue, initialValue)
    val acc = new MaxDateSQLMetric(name, param)
    sc.cleaner.foreach(_.registerAccumulatorForCleanup(acc))
    acc
  }

  def createMaxDateMetric(sc: SparkContext, name: String): MaxDateSQLMetric = {
    createMaxDateMetric(sc, name, _.max.toString,new  Date())
  }

  /**
   * created by sara
   * @param sc
   * @param name
   * @param stringValue
   * @param initialValue
   * @return
   */
  def createMinDateMetric(sc: SparkContext, name: String, stringValue: Seq[Date] => String,
                          initialValue: Date): MinDateSQLMetric = {
    val param = new MinDateSQLMetricParam(stringValue, initialValue)
    val acc = new MinDateSQLMetric(name,param)
    sc.cleaner.foreach(_.registerAccumulatorForCleanup(acc))
    acc
  }

  def createMinDateMetric(sc: SparkContext, name: String): MinDateSQLMetric = {
    createMinDateMetric(sc, name, _.min.toString,new  Date())
  }

  /**
   * created by sara
   * @param sc
   * @param name
   * @param stringValue
   * @param initialValue
   * @return
   */
  def createMaxMetric(sc: SparkContext, name: String, stringValue: Seq[Long] => String,
                      initialValue: Long): MaxSQLMetric = {
    val param = new MaxSQLMetricParam(stringValue, initialValue)
    val acc = new MaxSQLMetric(name, param)
    sc.cleaner.foreach(_.registerAccumulatorForCleanup(acc))
    acc
  }
  def createMaxMetric(sc: SparkContext, name: String): MaxSQLMetric = {
    createMaxMetric(sc, name, _.max.toString,0L)
  }

  /**
   * created by sara
   * @param sc
   * @param name
   * @param stringValue
   * @param initialValue
   * @return
   */
  def createMinMetric(sc: SparkContext, name: String, stringValue: Seq[Long] => String,
                      initialValue: Long): MinSQLMetric = {
    val param = new MinSQLMetricParam(stringValue, initialValue)
    val acc = new MinSQLMetric(name,param)
    sc.cleaner.foreach(_.registerAccumulatorForCleanup(acc))
    acc
  }
  def createMinMetric(sc: SparkContext, name: String): MinSQLMetric = {
    createMinMetric(sc, name, _.min.toString,1000000)
  }

  /**
   * created by sara
   * @param sc
   * @param name
   * @param stringValue
   * @param initialValue
   * @return
   */
  def createStatisticsMetric(sc: SparkContext, name: String,stringValue: Seq[mutable.HashMap[String,Long]] => String,
                             initialValue: mutable.HashMap[String,Long] with
                               mutable.SynchronizedMap[String, Long] with Serializable with scala.collection.generic.Clearable): HashMapSQLMetric = {
    val param = new HashMapSQLMetricParam(stringValue, initialValue)
    val acc = new HashMapSQLMetric(name, param)
    sc.cleaner.foreach(_.registerAccumulatorForCleanup(acc))
    acc
  }

  def createStatisticsMetric(sc: SparkContext, name: String): HashMapSQLMetric = {
    val map =  new mutable.HashMap[String,Long] with mutable.SynchronizedMap[String,Long] with Serializable with scala.collection.generic.Clearable
    createStatisticsMetric(sc, name, _.head.toString(),map)
  }

  /**
   * Create a metric to report the size information (including total, min, med, max) like data size,
   * spill size, etc.
   */
  def createSizeMetric(sc: SparkContext, name: String): LongSQLMetric = {
    val stringValue = (values: Seq[Long]) => {
      // This is a workaround for SPARK-11013.
      // We use -1 as initial value of the accumulator, if the accumulator is valid, we will update
      // it at the end of task and the value will be at least 0.
      val validValues = values.filter(_ >= 0)
      val Seq(sum, min, med, max) = {
        val metric = if (validValues.length == 0) {
          Seq.fill(4)(0L)
        } else {
          val sorted = validValues.sorted
          Seq(sorted.sum, sorted(0), sorted(validValues.length / 2), sorted(validValues.length - 1))
        }
        metric.map(Utils.bytesToString)
      }
      s"\n$sum ($min, $med, $max)"
    }
    // The final result of this metric in physical operator UI may looks like:
    // data size total (min, med, max):
    // 100GB (100MB, 1GB, 10GB)
    createLongMetric(sc, s"$name total (min, med, max)", stringValue, -1L)
  }

  /**
   * A metric that its value will be ignored. Use this one when we need a metric parameter but don't
   * care about the value.
   */
  val nullLongMetric = new LongSQLMetric("null", new LongSQLMetricParam(_.sum.toString, 0L))
}