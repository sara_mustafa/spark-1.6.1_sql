 Array_wr = csvread('E:\\sara csd\\Master\\In memory DB\\cluster\\95g_175q\\stages_wr_filtered.csv');
Array_exe = csvread('E:\\sara csd\\Master\\In memory DB\\cluster\\95g_175q\\stages_exe_filtered.csv');

  num_points = size(Array_exe,1);
 split_point = round(num_points*0.7);
 seq = randperm(num_points);
 Array_train_exe =Array_exe(seq(1:split_point),:);
 Array_test_exe = Array_exe(seq(split_point+1:end),:);

 X = Array_exe(:,2:end);
 Y = Array_exe(:,1);

  [IDX,D] = knnsearch(Array_wr(:,2:end),Array_test_exe(:,4:end),'K',3);
Wnew = zeros(size(Array_test_exe,1),1);
for k=1:size(Array_test_exe,1)
      Wnew(k)= (Array_wr(IDX(k,1),1)+Array_wr(IDX(k,2),1)+Array_wr(IDX(k,3),1))./3;
end;

scatter(Wnew,Array_test_exe(:,2),'+');
set(gca,'XScale','log');
set(gca,'YScale','log');
hold on
 axis equal
 grid on 
 line([1; 10.^9], [1; 10.^9])
 hold off
 xlabel('Predicted I/O writes')
 ylabel('Actual I/O writes')
 title('Stage Records Wrritten')
 
 pr_non = 0;
pr_dem = 0;
size=size(Array_test_exe,1);
meu = sum(Array_test_exe(:,2))./size;
for n=1:size
    pr_non =  pr_non + (Wnew(n)- Array_test_exe(n,2)).^2;
    pr_dem =  pr_dem + ( Array_test_exe(n,2) - meu).^2;
end
predictionRisk = 1 - (pr_non ./  pr_dem)

Array_test_exe(:,2)=Wnew;

 t = templateTree('MaxNumSplits',256,'Surrogate','on');
 Mdl = fitensemble(X,Y,'LSBoost',123,t,'Type','regression','LearnRate',0.50);

  Ynew = predict(Mdl, Array_test_exe(:,2:end));
scatter(Ynew,Array_test_exe(:,1),'+');
set(gca,'XScale','log');
set(gca,'YScale','log');
hold on
axis equal
grid on 
line([10.^-4; 10.^2], [10.^-4; 10.^2])
 hold off
 xlabel('Predicted Execution Time')
 ylabel('Actual Execution Time')
 title('Stage Execution Time (Min)')
pr_non = 0;
pr_dem = 0;
meu = sum(Array_test_exe(:,1)) ./ size(Array_test_exe,1);
for n=1:size(Array_test_exe,1)
    pr_non =  pr_non + (Ynew(n)- Array_test_exe(n,1)).^2;
    pr_dem =  pr_dem + ( Array_test_exe(n,1) - meu).^2;
end
predictionRisk = 1 - (pr_non ./  pr_dem)

