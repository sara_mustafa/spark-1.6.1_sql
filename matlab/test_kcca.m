clear;clc; close all force;
addpath('kcca_package');

eta_kcca = 1;
kapa_kcca = 1E-5;
reg=1E-5;
kapa_cca = 1;
kerneltype = 'gauss';   % kernel type
kernelpar = 1;  % kernel parameter

train = csvread('E:\\sara csd\\Master\\In memory DB\\cluster\\ec2-stages\\stages_train.csv');
test = csvread('E:\\sara csd\\Master\\In memory DB\\cluster\\ec2-stages\\stages_test.csv');

%  num_points = size(train,1);
% split_point = round(num_points*0.7);
% seq = randperm(num_points);
% Array_train =train(seq(1:split_point),:);
% Array_test = train(seq(split_point+1:end),:);

train_a =train(:, 4:end);
train_b =train(:, 2);
test_a =test(:, 4:end);
test_b =test(:, 2);
 
%  test_a =train_a;
%  test_b =train_b;
nt = size(test_a,1);
N = size(train_a,1);


[train_a_ker, omegaA] = kernel_expchi2(train_a,train_a);
[train_b_ker, omegaB] = kernel_expchi2(train_b,train_b);
[test_a_ker] = kernel_expchi2(test_a,train_a,omegaA);
[test_b_ker] = kernel_expchi2(test_b,train_b,omegaB);

[Wx, Wy, r] = kcanonca_reg_ver2(train_a_ker,train_b_ker,eta_kcca,kapa_kcca,0,0);
 
 test_b_ker_proj = test_b_ker*Wy;
 test_a_ker_proj = test_a_ker*Wx;


% [IDX,D] = knnsearch(Wx,test_a_ker_proj, 'K',3);
f =pdist2(test_a_ker_proj,Wx,'euclidean');
[B,IDX] = sort(f);

 res = zeros(nt,3);
for k=1:nt
   
   res(k,1)=( train_b(IDX(k,1),1)+train_b(IDX(k,2),1)+train_b(IDX(k,3),1))./3;
%      res(k,2)=( train_b(IDX(k,1),2)+train_b(IDX(k,2),2)+train_b(IDX(k,3),2))./3;
%       res(k,3)=( train_b(IDX(k,1),3)+train_b(IDX(k,2),3)+train_b(IDX(k,3),3))./3;
end;

figure
loglog(res(:,1), test_b(:,1),'.');
% scatter(res, test_b,'filled');
% set(gca,'XScale','log');
% set(gca,'YScale','log');
 hold on
 axis equal
 grid on 

 line([10.^-4; 10.^2], [10.^-4; 10.^2])
 hold off
 xlabel('Predicted')
 ylabel('Actual ')

pr_non = 0;
pr_dem = 0;
size=size(test,1)
meu = sum(test(:,1))./size;
for n=1:size
    pr_non =  pr_non + (res(n)- test(n,1)).^2;
    pr_dem =  pr_dem + ( test(n,1) - meu).^2;
end
predictionRisk = 1 - (pr_non ./  pr_dem)