package org.apache.spark.scheduler

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}
import java.util.concurrent.TimeUnit

import scala.collection.mutable

/**
 * Created by sara on 27/11/15.
 */
abstract class Histogram(tuplesNum: Int){
  val tuples: Int = tuplesNum
  def initHistogram(): Unit ={

  }
  def updateFreq(rangeLow: Int, rangHigh: Int, act:Int): Unit ={

  }
  def updateFreq(rangeLow: Date, rangHigh: Date, act:Int): Unit ={

  }
  def addBucket(key: String, value:Int): Unit ={

  }
}

class RangeHistogram (tuplesNum: Int, max: Int, min:Int ) extends Histogram(tuplesNum:Int){

  var interval: Int = 5
  val maxVal: Int = max
  val minVal:Int = min

  var buckets: mutable.ArrayBuffer[Bucket] = mutable.ArrayBuffer.empty

  override def initHistogram(): Unit = {
    var bucketsNum = 10
    if ((max - min) < interval)
      interval = interval / 2
    else
      while ((max - min) / bucketsNum < interval)
        bucketsNum -= 1
    var low = min
    var high = interval
    for (i <- 0 until bucketsNum) {
      if (i == bucketsNum - 1)
        buckets.insert(i, new Bucket(low, max, (tuples / bucketsNum) + (tuples % bucketsNum)))
      else
        buckets.insert(i, new Bucket(low, high, (tuples/ bucketsNum)))
      low = high
      high += interval
    }
  }

  override def updateFreq(rangeLow: Int, rangHigh: Int, act:Int): Unit ={
    // 1. get overlapped buckets
    var overlappedBucketsIndicies : mutable.ArrayBuffer[Int] = mutable.ArrayBuffer.empty
    for (i <-0 until buckets.size){
     if ((rangeLow >= buckets(i).bucketLow && rangeLow < buckets(i).bucketHigh) || (rangHigh >= buckets(i).bucketLow && rangHigh < buckets(i).bucketHigh)
     || (buckets(i).bucketLow >= rangeLow && buckets(i).bucketHigh <= rangHigh)){
       overlappedBucketsIndicies += i
     }
    }
    var bucketsFrac : mutable.ArrayBuffer[Float] = mutable.ArrayBuffer.empty
    var est = 0.0
    // 2. calculate estimated result size
    for(i <-0 until overlappedBucketsIndicies.size){
      val frac = (math.min(rangHigh,buckets(overlappedBucketsIndicies(i)).bucketHigh) - math.max(rangeLow,buckets(overlappedBucketsIndicies(i)).bucketLow) + 1).toFloat / (buckets(overlappedBucketsIndicies(i)).bucketHigh - buckets(overlappedBucketsIndicies(i)).bucketLow + 1)
      bucketsFrac += frac
      est += frac * buckets(overlappedBucketsIndicies(i)).bFreq
    }
    val esterr = act - est

    // 3. update freq
    for(i <-0 until overlappedBucketsIndicies.size){
      var x1= buckets(overlappedBucketsIndicies(i)).bFreq + 0.5 * esterr * bucketsFrac(i) * buckets(overlappedBucketsIndicies(i)).bFreq / est
      var freq = math.max(buckets(overlappedBucketsIndicies(i)).bFreq + 0.5 * esterr * bucketsFrac(i) * buckets(overlappedBucketsIndicies(i)).bFreq / est , 0)
      buckets(overlappedBucketsIndicies(i)).bFreq = freq
    }
  }
  override def toString(): String ={
    var str:String=""
    for (i <-0 until buckets.length)
      str+=buckets(i).toString+"\n"
    str
  }
}
class Bucket (start: Int, end: Int, freq: Double){

  var bucketLow:Int = start
  var bucketHigh:Int = end
  var bFreq: Double = freq

  override def toString(): String ={
    var str:String = bucketLow.toString+":"+bucketHigh.toString+":"+bFreq
    str
  }

}


class DateHistogram (tuplesNum: Int, max: Date, min:Date ) extends Histogram(tuplesNum:Int){

 var interval: Int = 30 // day
  val maxVal: Date = max
  val minVal:Date = min

  var buckets: mutable.ArrayBuffer[DateBucket] = mutable.ArrayBuffer.empty

  override def initHistogram(): Unit = {
    var bucketsNum: Int = 0
    var diffInDays:Long = ((maxVal.getTime() - minVal.getTime())/ (1000 * 60 * 60 * 24))

    bucketsNum = (diffInDays / interval).toInt - 1


    var cal = Calendar.getInstance()
    var low = min
    cal.setTime(low)
    cal.add(Calendar.MONTH, 1); // add 10 days
    var high = cal.getTime()
    for (i <- 0 until bucketsNum) {
      if (i == bucketsNum - 1)
        buckets.insert(i, new DateBucket(low, max, (tuples / bucketsNum) + (tuples % bucketsNum)))
      else
        buckets.insert(i, new DateBucket(low, high, (tuples/ bucketsNum)))
      low = high
      cal.setTime(high)
      cal.add(Calendar.MONTH, 1)
      high = cal.getTime()
    }
  }

  override def updateFreq(rangeLow: Date, rangHigh: Date, act:Int): Unit ={
    // 1. get overlapped buckets
    var overlappedBucketsIndicies : mutable.ArrayBuffer[Int] = mutable.ArrayBuffer.empty
    for (i <-0 until buckets.size){
      if (((rangeLow.after(buckets(i).bucketLow) || rangeLow.equals(buckets(i).bucketLow)) && rangeLow.before(buckets(i).bucketHigh)) || ((rangHigh.after(buckets(i).bucketLow) || rangHigh.equals(buckets(i).bucketLow)) && rangHigh.before(buckets(i).bucketHigh))
        || ( (buckets(i).bucketLow.after(rangeLow) || buckets(i).bucketLow.equals(rangeLow) ) && (buckets(i).bucketHigh.before(rangHigh) || buckets(i).bucketHigh.equals(rangHigh)))){
        overlappedBucketsIndicies += i
      }
    }
    var bucketsFrac : mutable.ArrayBuffer[Float] = mutable.ArrayBuffer.empty
    var est = 0.0
    // 2. calculate estimated result size
    for(i <-0 until overlappedBucketsIndicies.size){
      val frac = (math.min(rangHigh.getTime,buckets(overlappedBucketsIndicies(i)).bucketHigh.getTime) - math.max(rangeLow.getTime,buckets(overlappedBucketsIndicies(i)).bucketLow.getTime) + 1).toFloat / (buckets(overlappedBucketsIndicies(i)).bucketHigh.getTime - buckets(overlappedBucketsIndicies(i)).bucketLow.getTime + 1)
      bucketsFrac += frac
      est += frac * buckets(overlappedBucketsIndicies(i)).bFreq
    }
    val esterr = act - est

    // 3. update freq
    for(i <-0 until overlappedBucketsIndicies.size){
      var x1= buckets(overlappedBucketsIndicies(i)).bFreq + 0.5 * esterr * bucketsFrac(i) * buckets(overlappedBucketsIndicies(i)).bFreq / est
      var freq = math.max(buckets(overlappedBucketsIndicies(i)).bFreq + 0.5 * esterr * bucketsFrac(i) * buckets(overlappedBucketsIndicies(i)).bFreq / est , 0)
      buckets(overlappedBucketsIndicies(i)).bFreq = freq
    }
  }
  override def toString(): String ={
    var str:String=""
    for (i <-0 until buckets.length)
      str+=buckets(i).toString+"\n"
    str
  }
}
class DateBucket (start: Date, end: Date, freq: Double){

  var bucketLow:Date = start
  var bucketHigh:Date = end
  var bFreq: Double = freq
  val df = new SimpleDateFormat("yyyy-MM-dd")

  override def toString(): String ={
    df.format(bucketLow).toString+":"+df.format(bucketHigh).toString+":"+bFreq
  }

}

class DistinctHistogram(tuplesNum: Int) extends Histogram(tuplesNum: Int){
  var buckets:mutable.HashMap[String,Int] = mutable.HashMap()

  override def addBucket(key: String, value:Int){
    buckets.put(key,value)
  }
  override def toString(): String ={
    var str:String=""
    buckets.foreach {keyVal => str+=keyVal._1 + ":" + keyVal._2+"\n"}
    str
  }

}


